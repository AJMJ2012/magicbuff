﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System;
using Terraria.ID;
using Terraria.ModLoader;
using Terraria;
using System.Text.RegularExpressions;

namespace MagicBuff {
	public class MagicBuff : Mod {
		public MagicBuff() {
			Properties = new ModProperties {
				Autoload = true
			};
		}

		public static bool TooltipsPlusLoaded = false;
		public override void Load() {
			TooltipsPlusLoaded = ModLoader.GetMod("TooltipsPlus") != null;
			if (TooltipsPlusLoaded) { InsertStuff(); }
		}

		public static void InsertStuff() {
			TooltipsPlus.Config.TooltipGroups["ItemBuffs"].Insert(28, "ManaDamage");
		}
	}

	public class GItem : GlobalItem {
		public override void GetWeaponDamage(Item item, Player player, ref int damage) {
			if (item.damage > 0 || item.type == 0 || item.summon || (int)(item.mana * player.manaCost) <= 0) { return; }
			damage += (int)(item.mana * player.manaCost);
		}

		public override void ModifyTooltips(Item item, List<TooltipLine> tooltips) {
			Player player = Main.player[Main.myPlayer];
			if (!MagicBuff.TooltipsPlusLoaded || item.damage <= 0 || item.type == 0 || item.summon || (int)(item.mana * player.manaCost) <= 0) { return; }
			TooltipLine tooltip = new TooltipLine(mod, "ManaDamage", "+" + (int)(item.mana * player.manaCost) + " damage from mana usage");
			tooltip.isModifier = true;
			tooltips.Add(tooltip);
		}
	}
}
